{
  edition = 201909;

  description = "Oberon RISC machine, emulated for x86_64";

  inputs = {
    dhall-haskell.uri =
      "git+https://github.com/dhall-lang/dhall-haskell?ref=flake";
    genode-depot.uri = "git+https://gitea.c3d2.de/ehmry/genode-depot.git";
    genodepkgs.url = "git+https://git.sr.ht/~ehmry/genodepkgs";
    libretro-genode.uri = "git+https://gitea.c3d2.de/ehmry/libretro-genode";
  };

  outputs =
    { self, genodepkgs, dhall-haskell, genode-depot, libretro-genode, nixpkgs }:
    let
      mkOutput = { system, localSystem, crossSystem }:

        let
          thisSystem = builtins.getAttr system;
          mergeManifests = manifests:
            nixpkgs.legacyPackages.${localSystem}.writeTextFile {
              name = "manifest.dhall";
              text = with builtins;
                let f = head: input: "${head},${input.pname}=${input.manifest}";
                in (foldl' f "{" manifests) + "}";
            };
        in {
          defaultPackage.${system} = with thisSystem genodepkgs.packages;
            genodepkgs.apps.${system}.nova-iso.function {
              MANIFEST = mergeManifests ((with genodepkgs.packages.${system}; [
                genode.base-nova
                genode.os
                genode.gems
              ]) ++ (with genode-depot.packages.${system}; [ driver_manager usb_drv intel_fb_drv  boot_fb_drv vesa_drv ]));
            } ./boot.dhall;
        };

      crossOutput = mkOutput {
        localSystem = "x86_64-linux";
        crossSystem = "x86_64-genode";
        system = "x86_64-linux-x86_64-genode";
      };

    in crossOutput // {
      defaultPackage.x86_64-linux =
        crossOutput.defaultPackage.x86_64-linux-x86_64-genode;
    };

}
